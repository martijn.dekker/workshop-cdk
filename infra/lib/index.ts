import * as cdk from "aws-cdk-lib";
import { WidgetServiceStack } from "./widget-service-stack";

const app = new cdk.App();

new WidgetServiceStack(app, "Widgets", {
  env: {
    account: app.node.tryGetContext("accountId"),
    region: "eu-west-1",
  },
});

app.synth();
